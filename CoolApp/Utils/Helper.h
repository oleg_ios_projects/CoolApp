//
//  Helper.h
//  NNMotors
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Constants.h"

@interface Helper : NSObject

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (UIImage*)imageWithColor:(UIColor*)color;
+ (NSString*)stringFromDate:(NSDate*)dateTime format:(NSString*)format;
+ (UIColor *)randomColor;
+ (int)getRandomNumberBetween:(int)from to:(int)to;
@end
