//
//  Constants.h
//  NNMotors
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#pragma mark - Network
#define SERVER_URL @"http://stnn.platformasoft.ru"
#define INFO_REQUEST_URL SERVER_URL @"/api/external/info"
#define AUTH_REQUEST_URL SERVER_URL @"/api/external/auth"
#define HISTORY_REQUEST_URL SERVER_URL @"/api/external/history"
#define SEND_REQUEST_URL SERVER_URL @"/api/external/send"
#define CHECK_REQUEST_URL SERVER_URL @"/api/external/check"
#define SERVER_LOGIN @"client"
#define SERVER_PASSWORD @"stnn!2016"

#pragma mark - Font
#define REGULAR_FONT @"Helvetica"
#define BOLD_FONT @"Helvetica-Bold"

#pragma mark - Color
#define RED_GENERAL_COLOR @"#FF3830"
#define BLUE_GENERAL_COLOR @"#007AFF"
#define GREEN_GENERAL_COLOR @"#4CD964"
#define WHITE_TEXT_COLOR @"#FFFFFF"

#define LIGHT_BLUE_VIDEOS_COLOR @"#5AC8FA"
#define YELLOW_NOTES_COLOR @"#FFCC00"
#define ORANGE_BOOKS_COLOR @"#FF9500"
#define RED_APPNEWS_COLOR @"#FF2D55"
#define BLUE_SAFARI_COLOR @"#007AFF"
#define LIGHT_GREEN_MESSAGES_COLOR @"#4CD964"
#define RED_CALENDAR_COLOR @"#FF3B30"
#define DARK_GRAY_SETTINGS_COLOR @"#8E8E93"
#define LIGHT_GRAY_BACKGROUND_COLOR @"#EFEFF4"
#define GRAY_LINES_COLOR @"#CECED2"
#define BLACK_TEXT_COLOR @"#000000"
#define BLUE_LINKS_COLOR @"#007AFF"

#define DARK_GREEN_COLOR @"#13A438"

#pragma mark - Keys
#define FIRST_LAUNCH_KEY @"FirstLaunch"

#pragma mark - Enums
typedef enum
{
    PinStateUnknown = 0,
    PinStateActive
    
} PinState;

typedef enum{
    
    SessionStateUnknown = 0,
    SessionStateInvalid,
    SessionStateActive,
    SessionStateLocked
    
} SessionState;

#endif /* Constants_h */
