//
//  Helper.m
//  NNMotors
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "Helper.h"

@implementation Helper

// Assumes input like "#00FF00" (#RRGGBB).
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    NSLog(@"%f",((rgbValue & 0xFF0000) >> 16)/255.0);
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(UIImage*)imageWithColor:(UIColor*)color {
    CGRect rect = CGRectMake(0, 0, 1.0, 1.0);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    if (color) {
        [color setFill];
    } else {
        [[self colorFromHexString:WHITE_TEXT_COLOR] setFill];
    }
    CGContextFillRect(context, rect);
    UIImage* image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSString*)stringFromDate:(NSDate*)dateTime format:(NSString*)format {
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc]init];
    if ([format length]==0) {
        format = @"HH:mm dd-MM-yyyy";
    }
    [dateFormatter setDateFormat:format];
    return [dateFormatter stringFromDate:dateTime];
}

+ (UIColor *)randomColor {
    int red = [self getRandomNumberBetween:0 to:255];
    int green = [self getRandomNumberBetween:0 to:255];
    int blue = [self getRandomNumberBetween:0 to:255];
    
    return [UIColor colorWithRed: red/255.0
                           green: green/255.0
                            blue: blue/255.0
                           alpha:1.0];
}

+(int)getRandomNumberBetween:(int)from to:(int)to {
    
    return (int)from + arc4random() % (to-from+1);
}

@end
