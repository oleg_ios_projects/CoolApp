//
//  AppDelegate.h
//  CoolApp
//
//  Created by iMAC on 15.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *loginNavigationController;
@property (strong, nonatomic) UINavigationController *applicationNavigationController;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
- (void)updateAppState;


@end

