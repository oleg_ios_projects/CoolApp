//
//  main.m
//  CoolApp
//
//  Created by iMAC on 15.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
