//
//  UIGravityFun.m
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIGravityFun.h"
#import <CoreMotion/CoreMotion.h>
#import "HeaderFilesControllers.h"

@interface UIGravityFun ()
@property (nonatomic) UIDynamicAnimator* animator;
@property (nonatomic) UIGravityBehavior* gravity;
@property (nonatomic) UICollisionBehavior* collision;
@property (nonatomic) UIDynamicItemBehavior* itemBehavior;

@property (nonatomic) UIView* previousItem;

@property (nonatomic) UIInterfaceOrientation orientation;

@property (nonatomic) CMMotionManager* motionManager;
@property (nonatomic) NSOperationQueue* motionQueue;

@property (nonatomic) NSMutableArray* items;

@property (weak, nonatomic) IBOutlet UIView *generalView;
@end

@implementation UIGravityFun



- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavBarWithTitle:@"Гравитация"];
    
    
    self.items = [[NSMutableArray alloc]init];
    
    [self createAnimator];
    [self createManager];
    
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    
}

-(void)viewDidAppear:(BOOL)animated {
    [self generateItemsCount:25];
        [self.motionManager startDeviceMotionUpdatesToQueue:self.motionQueue withHandler:^(CMDeviceMotion * _Nullable motion, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self gravityUpdatedMotion:motion withError:error];
        });
    }];
    [self.itemBehavior addItem:self.generalView];
}

-(void)viewDidDisappear:(BOOL)animated {
    [self.motionManager stopDeviceMotionUpdates];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Add Views
- (bool) isNotCollideWithRect:(CGRect)rect Array:(NSArray*)array{
    for (UIView* item in array) {
        if (CGRectIntersectsRect(rect, item.frame)) {
            return false;
        }
    }
    return true;
}

- (CGRect) randomFrameWithMaxX:(int)maxX
                          MaxY:(int)maxY
                      MaxWidth:(CGFloat)width
                     MaxHeight:(CGFloat)height
{
    CGRect frame = CGRectMake(maxX/2, maxY/2, width, height);
    while (![self isNotCollideWithRect:frame Array:self.items]) {
        CGFloat frameX = 1.0*(arc4random()%maxX);
        CGFloat frameY = 1.0*(arc4random()%maxY);
        frame = CGRectMake(frameX, frameY, width, height);
    }
    return frame;
}

- (UIView*) addItemWithFrame: (CGRect)frame Color:(UIColor*)color {
    UIView* newItem = [[UIView alloc]initWithFrame:frame];
    [newItem.layer setCornerRadius:frame.size.height/2.0];
    [newItem setBackgroundColor:color];
    [self.generalView addSubview:newItem];
    return newItem;
}

-(void) generateItemsCount:(int)count {
    for (int i=0; i<count; i++) {
        CGRect frame = [self randomFrameWithMaxX:self.generalView.frame.size.width*0.9 MaxY:self.generalView.frame.size.height*0.9 MaxWidth:30 MaxHeight:30];
        UIView* newItem = [self addItemWithFrame:frame Color:[Helper randomColor]];
        [self addViewToBehaviorView:newItem];
        [self.items addObject:newItem];
        //[self chainingItemsWithItem:newItem];
    }
}


#pragma mark - Behavior methods
- (void) addViewToBehaviorView:(UIView*)view {
    [self.gravity addItem:view];
    [self.collision addItem:view];
    [self.itemBehavior addItem:view];
}

- (void) chainingItemsWithItem:(UIView*)item {
    UIAttachmentBehavior *attach = [UIAttachmentBehavior alloc];
    if (self.previousItem.frame.origin.x == 0) {
        [attach initWithItem:item attachedToAnchor:CGPointMake(50, 50)];
    } else {
        [attach initWithItem:item attachedToItem:self.previousItem];
    }
    [attach setLength:61];
    [attach setDamping:0.5];
    [self.animator addBehavior:attach];
    self.previousItem = item;
}

- (void) createAnimator {
    self.animator = [[UIDynamicAnimator alloc]initWithReferenceView:self.generalView];
    self.gravity = [[UIGravityBehavior alloc]init];
    self.collision = [[UICollisionBehavior alloc] init];
    self.itemBehavior = [[UIDynamicItemBehavior alloc]init];
    
    [self.gravity setGravityDirection:CGVectorMake(0, 0.8)];
    [self.animator addBehavior:self.gravity];
    
    [self.collision setTranslatesReferenceBoundsIntoBoundary:YES];
    [self.animator addBehavior:self.collision];
    
    [self.itemBehavior setFriction:0.1];
    [self.itemBehavior setElasticity:0.9];
    [self.animator addBehavior:self.itemBehavior];
}

- (void) createManager {
    self.motionManager =[[CMMotionManager alloc]init];
    self.motionQueue = [[NSOperationQueue alloc]init];
    
    [self.motionManager setDeviceMotionUpdateInterval:1.0/30.0];
}


-(void) gravityUpdatedMotion:(CMDeviceMotion*) motion withError:(NSError*) error {
    
    if (error) {
        NSLog(@"Error with updated motion");
        return;
    }
    
    CGFloat x = motion.gravity.x;
    CGFloat y = motion.gravity.y;
    CGPoint point = CGPointMake(x, y);
    
    
    self.orientation = [[UIApplication sharedApplication] statusBarOrientation];
    switch (self.orientation) {
        case UIInterfaceOrientationPortrait:
        {
            
        }
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            point.x *=-1;
            point.y *=-1;
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
        {
            point.x = 0 - point.y;
            point.y = point.x + point.y;
        }
            break;
        case UIInterfaceOrientationLandscapeRight:
        {
            point.y = 0 - point.x;
            point.x = point.y + point.x;
        }
            break;
        default:
            break;
    }
    
    [self.gravity setGravityDirection:CGVectorMake(point.x, 0-point.y)];
}

@end
