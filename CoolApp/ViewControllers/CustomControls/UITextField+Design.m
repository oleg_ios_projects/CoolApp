//
//  UITextField+Design.m
//  A3mobile
//
//  Created by iMAC on 25.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UITextField+Design.h"
#import "FAKFontAwesome.h"
#import "Helper.h"
#import "Constants.h"

@implementation UITextField (Design)

-(void)setBottomBorderWidth:(CGFloat) borderWidth {
    CALayer *border = [CALayer layer];
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, self.frame.size.height-borderWidth, self.frame.size.width*0.9, self.frame.size.height);
    border.borderWidth = borderWidth;
    [self.layer addSublayer:border];
    self.layer.masksToBounds = YES;
}


@end
