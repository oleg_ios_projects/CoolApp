//
//  UISecureCodeView.h
//  A3mobile
//
//  Created by iMAC on 04.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class UISecureCodeView;

@protocol UISecureCodeViewDelegate <NSObject>

- (void)secureCodeViewFull:(UISecureCodeView*)view;

@end

@interface UISecureCodeView : UIView<UITextViewDelegate>

@property(nonatomic)NSInteger lenght;
@property(nonatomic)id<UISecureCodeViewDelegate> delegate;
@property(nonatomic)UIView* keyboardToolbarView;
@property(nonatomic)NSString* text;

@property(nonatomic)BOOL enableEditing;

- (BOOL)textBecomeFirstResponder;

@end

