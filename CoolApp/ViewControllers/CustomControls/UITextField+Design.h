//
//  UITextField+Design.h
//  A3mobile
//
//  Created by iMAC on 25.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Design)
-(void)setBottomBorderWidth:(CGFloat) borderWidth;
@end
