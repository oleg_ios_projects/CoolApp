//
//  UIViewController+NavBar.m
//  A3mobile
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIViewController+NavBar.h"
#import "FAKFontAwesome.h"
#import "Helper.h"

@implementation UIViewController (NavBar)

-(void)setupNavBarWithTitle:(NSString*)title {
    self.navigationItem.title = title;
    self.navigationItem.backBarButtonItem = [self backMenuBarButtonItem];
}

#pragma mark - UIBarButtonItem creator
-(UIBarButtonItem*)leftMenuBarButtonItem {
    //return nil;
    FAKFontAwesome* icon = [FAKFontAwesome barsIconWithSize:20];
     [icon addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor]];
     UIImage* image = [icon imageWithSize:CGSizeMake(20,20)];
     return [[UIBarButtonItem alloc] initWithImage:image
     style:UIBarButtonItemStylePlain
     target:self
     action:@selector(leftSideMenuButtonPressed:)];
}

-(UIBarButtonItem*)rightMenuBarButtonItem {
    return nil;
    /*return [[UIBarButtonItem alloc] initWithTitle:@"Меню"
     style:UIBarButtonItemStyleBordered
     target:self
     action:@selector(rightSideMenuButtonPressed:)];*/
}

-(UIBarButtonItem*)backMenuBarButtonItem {
    /*FAKFontAwesome* icon = [FAKFontAwesome chevronLeftIconWithSize:20];
     [icon addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor]];
     UIImage* image = [icon imageWithSize:CGSizeMake(20,20)];
     UIBarButtonItem* result = [[UIBarButtonItem alloc] initWithImage:image
     style:UIBarButtonItemStyleBordered
     target:self
     action:@selector(backButtonPressed:)];*/
    UIBarButtonItem* result = [[UIBarButtonItem alloc] init];
    result.title = NSLocalizedString(@"IDS_GENERAL_BACKTITLE", @"");
    return result;
}

#pragma mark - Open view methods
- (void)putViewController:(UIViewController*)vc
{
    if(vc != nil)
    {
        UINavigationController *navigationController = self.navigationController;
        NSArray *controllers = [NSArray arrayWithObject:vc];
        navigationController.viewControllers = controllers;
    }
}

#pragma mark - Color methods 
- (void)setupNavBarWithColor:(UIColor*)color {
    UIImage *image = [Helper imageWithColor:color];
    UINavigationBar* resultBar = self.navigationController.navigationBar;
    [resultBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [resultBar setBarStyle:UIBarStyleDefault];
    [resultBar setShadowImage:[UIImage new]];
    [resultBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [resultBar setTintColor:[UIColor whiteColor]];
    [resultBar setTranslucent:YES];
}

@end
