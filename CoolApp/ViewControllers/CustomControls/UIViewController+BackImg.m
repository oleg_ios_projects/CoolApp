//
//  UIViewController+BackImg.m
//  A3mobile
//
//  Created by iMAC on 18.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIViewController+BackImg.h"

@implementation UIViewController (BackImg)

- (void)setupBackgroundWithImage:(UIImage*)image {
    CGRect imgFrame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    UIImageView* imageView = [[UIImageView alloc]initWithFrame:imgFrame];
    [imageView setBackgroundColor:[UIColor clearColor]];
    [imageView setImage:image];
    [imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.view addSubview:imageView];
    [self.view sendSubviewToBack:imageView];
    [self.view setBackgroundColor:[UIColor clearColor]];
}

- (void)setupBackgroundDefaultImage {
    UIImage* image = [UIImage imageNamed:@"background_blur"];
    [self setupBackgroundWithImage:image];
}


@end
