//
//  UITableViewController+DataArray.m
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIViewController+TableDataArray.h"
#import <objc/runtime.h>

#define kDataArray @"kDataArray"

@implementation UIViewController (TableDataArray)

-(void)setDataArray:(NSArray *)dataArray {
objc_setAssociatedObject(self, kDataArray, dataArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);

}

-(NSArray*)dataArray {
    return objc_getAssociatedObject(self, kDataArray);
}

-(void)updateDataArray {
    self.dataArray = [NSArray array];
}

@end
