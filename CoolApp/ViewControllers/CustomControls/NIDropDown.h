//
//  NIDropDown.h
//  A3mobile
//
//  Created by iMAC on 06.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    NIDropDownDirectionDown,
    NIDropDownDirectionUp
} NIDropDownDirection;

@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NIDropDown *) sender;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NIDropDownDirection animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic) NIDropDownDirection animationDirection;
@property (nonatomic) NSString* resultString;
-(void)hideDropDownOnView:(UIView*)view button:(UIButton *)button;
- (id)showDropDownOnView:(UIView*)view button:(UIButton *)button height:(CGFloat *)height titleArray:(NSArray *)array imageArray:(NSArray *)imgArray direction:(NIDropDownDirection)direction;
@end
