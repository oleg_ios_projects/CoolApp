//
//  UITextField+Format.m
//  A3mobile
//
//  Created by iMAC on 25.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UITextField+Format.h"

@implementation UITextField (Format)

-(void)selectTextAtRange:(NSRange)range {
    UITextPosition *start = [self positionFromPosition:[self beginningOfDocument] offset:range.location];
    UITextPosition *end = [self positionFromPosition:start offset:range.length];
    [self setSelectedTextRange:[self textRangeFromPosition:start toPosition:end]];
}

-(BOOL)allowNumberCharactersAtRange:(NSRange)range string:(NSString*)string length:(NSNumber*)length {
    NSString *changedString = [self.text stringByReplacingCharactersInRange:range withString:string];
    
    if (string.length!=0 && [string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound) {
        return NO;
    }
    
    if(range.length == 1 && // Only do for single deletes
       string.length < range.length &&
       [[self.text substringWithRange:range] rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789"]].location == NSNotFound)
    {
        // Something was deleted.  Delete past the previous number
        NSInteger location = changedString.length-1;
        if(location > 0)
        {
            for(; location > 0; location--)
            {
                if(isdigit([changedString characterAtIndex:location]))
                {
                    break;
                }
            }
            changedString = [changedString substringToIndex:location];
        }
    }
    if (length) {
        if (changedString.length<=[length intValue]) {
            self.text = changedString;
            [self selectTextAtRange:NSMakeRange(range.location+string.length, 0)];
            if ([self.text length]==[length intValue]) {
                [self resignFirstResponder];
            }
        }
    }
    return NO;

}

-(BOOL)allowNumberCharactersAtRange:(NSRange)range string:(NSString*)string {
    return [self allowNumberCharactersAtRange:range string:string length:nil];
}

-(BOOL)allowCharactersAtRange:(NSRange)range string:(NSString*)string length:(int)length {
    NSString *changedString = [self.text stringByReplacingCharactersInRange:range withString:string];
        if (changedString.length<=length) {
            self.text = changedString;
            [self selectTextAtRange:NSMakeRange(range.location+string.length, 0)];
            if ([self.text length]==length) {
                [self resignFirstResponder];
            }
        }
   return NO;
    
}


@end
