//
//  UITableViewController+DataArray.h
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - General key for cells
#define KEY_CELL_ID @"kCellID"
#define KEY_CELL_TITLE @"kCellTitle"
#define KEY_CELL_DETAIL @"kCellDetail"
#define KEY_CELL_VALUE @"kCellValue"
#define KEY_CELL_ACTION @"kCellAction"

#define KEY_SECTION_ID @"kSectionID"
#define KEY_SECTION_TITLE @"kSectionTtl"
#define KEY_SECTION_DESCRIPTION @"kSectionDecription"
#define KEY_SECTION_ACTION @"kSectionAction"
#define KEY_SECTION_CONTENT @"kSectionContent"
#define KEY_SECTION_FOOTER @"kSectionFooter"
#define KEY_SECTION_FOOTER_DESCRIPTION @"kSectionFooterDecription"

@interface UIViewController (TableDataArray)


@property (nonatomic, strong) NSArray* dataArray;
- (void) updateDataArray;
@end
