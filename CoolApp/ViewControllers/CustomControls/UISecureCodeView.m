//
//  UISecureCodeView.m
//  A3mobile
//
//  Created by iMAC on 04.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UISecureCodeView.h"
#import "Helper.h"
#import "Constants.h"

#define POINT_DISTANCE 25.0
#define POINT_SIZE 20.0
@implementation UISecureCodeView
{
    NSMutableArray * points;
    UITextView * valueField;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    valueField = [[UITextView alloc] initWithFrame:CGRectMake(-1,-1,1,1)];
    valueField.alpha = 1.0;
    valueField.keyboardType = UIKeyboardTypeNumberPad;
    valueField.delegate = self;
    
    self.clipsToBounds = YES;
    
    [self addSubview:valueField];
    
    UITapGestureRecognizer *tapRecogniser = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleTap:)];
    [tapRecogniser setNumberOfTouchesRequired:1];
    [self addGestureRecognizer:tapRecogniser];
}

-(void)handleTap:(UIGestureRecognizer *)gestureRecognizer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [valueField becomeFirstResponder];
    });
    
}

- (BOOL)textBecomeFirstResponder
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [valueField becomeFirstResponder];
    });
    
    
    return NO;
}

- (NSString*)text
{
    return valueField.text;
}

- (void)setEnableEditing:(BOOL)enableEditing
{
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        valueField.editable = enableEditing;
        
        if (enableEditing == NO) {
            valueField.text = @"";
        }
    });
    
}

- (BOOL)enableEditing
{
    return valueField.editable;
}

-(void)setText:(NSString *)text
{
    valueField.text = text;
    [self textFieldDidChange:valueField];
}

- (void)setKeyboardToolbarView:(UIView *)keyboardToolbarView
{
    valueField.inputAccessoryView = keyboardToolbarView;
}

- (UIView*)keyboardToolbarView
{
    return valueField.inputAccessoryView;
}

-(void)setLenght:(NSInteger)lenght
{
    _lenght = lenght;
    
    if(points.count>0)
        [points makeObjectsPerformSelector:@selector(removeFromSuperview)];
    points = [[NSMutableArray alloc] init];
    
    CGFloat leftMargin = (self.bounds.size.width - (lenght*POINT_SIZE+(lenght-1)*POINT_DISTANCE))/2;
    
    for(NSInteger i = 0; i<lenght; i++)
    {
        UIView * v = [[UIView alloc] initWithFrame:CGRectMake(leftMargin+(POINT_DISTANCE+POINT_SIZE)*i,
                                                              (self.frame.size.height-POINT_SIZE)/2,
                                                              POINT_SIZE,
                                                              POINT_SIZE)];
        
        v.layer.cornerRadius = v.frame.size.height/2;
        v.layer.borderColor = [UIColor lightGrayColor].CGColor;
        v.layer.borderWidth = 0.5;
        v.backgroundColor = [UIColor clearColor];
        
        [self addSubview:v];
        [points addObject:v];
    }
    
}

-(void)textFieldDidChange:(UITextView*)textField
{
    for(NSInteger i = 0; i<points.count; i++)
    {
        if(textField.text.length > i)
        {
            ((UIView*)[points objectAtIndex:i]).backgroundColor = [Helper colorFromHexString:BLUE_GENERAL_COLOR];
        }
        else
        {
            ((UIView*)[points objectAtIndex:i]).backgroundColor = [UIColor clearColor];
        }
    }
    
    if(textField.text.length == self.lenght)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if(self.delegate)
                [self.delegate secureCodeViewFull:self];
        });
    }
    
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if(textView.text.length >= self.lenght && text.length>0)
        return NO;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self textFieldDidChange:textView];
    });
    return YES;
}



@end
