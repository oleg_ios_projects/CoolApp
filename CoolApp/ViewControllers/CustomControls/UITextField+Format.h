//
//  UITextField+Format.h
//  A3mobile
//
//  Created by iMAC on 25.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Format)

-(void)selectTextAtRange:(NSRange)range;
-(BOOL)allowNumberCharactersAtRange:(NSRange)range string:(NSString*)string length:(NSNumber*)length;
-(BOOL)allowNumberCharactersAtRange:(NSRange)range string:(NSString*)string;
-(BOOL)allowCharactersAtRange:(NSRange)range string:(NSString*)string length:(int)length;
@end
