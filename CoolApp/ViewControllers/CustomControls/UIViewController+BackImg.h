//
//  UIViewController+BackImg.h
//  A3mobile
//
//  Created by iMAC on 18.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BackImg)
- (void)setupBackgroundWithImage:(UIImage*)image;
- (void)setupBackgroundDefaultImage;
@end
