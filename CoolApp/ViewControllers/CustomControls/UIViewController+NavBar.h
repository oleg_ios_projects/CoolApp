//
//  UIViewController+NavBar.h
//  A3mobile
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NavBar)
- (void)setupNavBarWithTitle:(NSString*)title;
- (void)putViewController:(UIViewController*)vc;
- (void)setupNavBarWithColor:(UIColor*)color;
@end
