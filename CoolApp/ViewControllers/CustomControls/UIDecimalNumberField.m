//
//  UIDecimalNumberField.m
//  A3mobile
//
//  Created by iMAC on 07.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIDecimalNumberField.h"
#import "BlocksKit+UIKit.h"

@implementation UIDecimalNumberField

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.keyboardType = UIKeyboardTypeDecimalPad;
    
    
    [self setBk_shouldChangeCharactersInRangeWithReplacementStringBlock:^BOOL(UITextField *textField, NSRange range, NSString *string) {
        
        NSLog(@"shouldChange");
        if (range.length>0 && [string length]==0) {
            return YES;
        }
        if ([string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"0123456789.,"]].location == NSNotFound) {
            return NO;
        }
        return YES;
    }];
}

- (void)setText:(NSString *)text
{
    [super setText:[text stringByReplacingOccurrencesOfString:@"." withString:@","]];
}

@end
