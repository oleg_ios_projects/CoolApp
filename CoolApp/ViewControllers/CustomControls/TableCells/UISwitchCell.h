//
//  UISwitchCell.h
//  A3mobile
//
//  Created by iMAC on 08.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UISwitchCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *switchControl;
+(CGFloat)height;
@end
