//
//  UITextFieldCell.h
//  A3mobile
//
//  Created by iMAC on 07.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextFieldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textField;

+(CGFloat)height;
@end
