//
//  UITextFieldCell.m
//  A3mobile
//
//  Created by iMAC on 07.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UITextFieldCell.h"

@implementation UITextFieldCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(CGFloat)height {
    return 44;
}

@end
