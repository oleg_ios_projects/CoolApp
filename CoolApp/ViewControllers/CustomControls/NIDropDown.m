//
//  NIDropDown.m
//  A3mobile
//
//  Created by iMAC on 06.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "NIDropDown.h"
#import "Helper.h"
#import "Constants.h"

@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, strong) UIView *rootView;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@property(nonatomic) CGFloat tableWidth;
@property(nonatomic) CGFloat leftCornerX;
@property(nonatomic) CGFloat leftCornerY;
@end

@implementation NIDropDown
@synthesize table;
@synthesize btnSender;
@synthesize rootView;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;
@synthesize tableWidth;
@synthesize leftCornerX;
@synthesize leftCornerY;

- (id)showDropDownOnView:(UIView*)view button:(UIButton *)button height:(CGFloat *)height titleArray:(NSArray *)array imageArray:(NSArray *)imgArray direction:(NIDropDownDirection)direction {
    btnSender = button;
    rootView = view;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = [button convertRect:button.bounds toView:view];
        tableWidth = btn.size.width*2;
        leftCornerX = btn.origin.x-btn.size.width/2;
        leftCornerY = btn.origin.y;
        self.list = [NSArray arrayWithArray:array];
        self.imageList = [NSArray arrayWithArray:imgArray];
        switch (direction) {
            case NIDropDownDirectionUp:
            {
                self.frame = CGRectMake(leftCornerX, leftCornerY, tableWidth, 0);
                self.layer.shadowOffset = CGSizeMake(-5, -5);
                break;
            }
            case NIDropDownDirectionDown:
            default:
            {
                self.frame = CGRectMake(leftCornerX, leftCornerY+btn.size.height, tableWidth, 0);
                self.layer.shadowOffset = CGSizeMake(-5, 5);
                break;
            }
        }
        /*
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        */
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, tableWidth, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        table.backgroundColor = [Helper colorFromHexString:WHITE_TEXT_COLOR];
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor = [UIColor grayColor];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        switch (direction) {
            case NIDropDownDirectionUp:
            {
                self.frame = CGRectMake(leftCornerX, leftCornerY-*height, tableWidth, *height);
                break;
            }
            case NIDropDownDirectionDown:
            default:
            {
                self.frame = CGRectMake(leftCornerX, leftCornerY+btn.size.height, tableWidth, *height);
                break;
            }
        }
        /*
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, *height);
        }
        */
        table.frame = CGRectMake(0, 0, tableWidth, *height);
        [UIView commitAnimations];
        [view addSubview:self];
        [self addSubview:table];
    }
    return self;
}

-(void)hideDropDownOnView:(UIView*)view button:(UIButton *)button {
    CGRect btn = [button convertRect:button.bounds toView:view];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    switch (animationDirection) {
        case NIDropDownDirectionUp:
        {
            self.frame = CGRectMake(leftCornerX, leftCornerY, tableWidth, 0);
            break;
        }
        case NIDropDownDirectionDown:
        default:
        {
            self.frame = CGRectMake(leftCornerX, leftCornerY+btn.size.height, tableWidth, 0);
            break;
        }
    }
    /*
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height, btn.size.width, 0);
    }
    */
    table.frame = CGRectMake(0, 0, tableWidth, 0);
    [UIView commitAnimations];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        [cell.textLabel setTextAlignment:NSTextAlignmentCenter];
    }
    if ([self.imageList count] == [self.list count]) {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        cell.imageView.image = [imageList objectAtIndex:indexPath.row];
    } else if ([self.imageList count] > [self.list count]) {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        if (indexPath.row < [imageList count]) {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    } else if ([self.imageList count] < [self.list count]) {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        if (indexPath.row < [imageList count]) {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    }
    
    cell.textLabel.textColor = [UIColor blackColor];
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = v;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDownOnView:rootView button:btnSender];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    self.resultString = c.textLabel.text;
    for (UIView *subview in btnSender.subviews) {
        if ([subview isKindOfClass:[UIImageView class]]) {
            [subview removeFromSuperview];
        }
    }
    imgView.image = c.imageView.image;
    imgView = [[UIImageView alloc] initWithImage:c.imageView.image];
    imgView.frame = CGRectMake(5, 5, 25, 25);
    [btnSender addSubview:imgView];
    [self myDelegate];
}

- (void) myDelegate {
    [self.delegate niDropDownDelegateMethod:self];
}

-(void)dealloc {
    //    [super dealloc];
    //    [table release];
    //    [self release];
}

@end
