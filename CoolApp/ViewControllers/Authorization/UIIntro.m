//
//  UIIntro.m
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIIntro.h"
#import <NDParallaxIntroView/NDIntroView.h>
#import "HeaderFilesControllers.h"
#import "FAKFontAwesome.h"
#import "AppDelegate.h"

@interface UIIntro ()<NDIntroViewDelegate>
@property (nonatomic, strong) NDIntroView* introView;

@property(nonatomic) NSArray* pageContentArray;
@end

@implementation UIIntro

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.pageContentArray = @[@{kNDIntroPageTitle : @"Клевое приложение!",
                                kNDIntroPageDescription : @"На самом деле это самое клевое приложение, которое ты когда-либо видел.",
                                kNDIntroPageImageName : @"Intro_1"
                                },
                              @{kNDIntroPageTitle : @"Поразительные\nвозможности!",
                                kNDIntroPageDescription : @"С этим клевым приложением ты можешь делать все и ничего одновременно.",
                                kNDIntroPageImageName : @"Intro_2"
                                },
                              @{kNDIntroPageTitle : @"Совершенно новое!",
                                kNDIntroPageDescription : @"Такого приложение больше нигде не найти.",
                                kNDIntroPageImageName : @"Intro_3"
                                },
                              @{kNDIntroPageTitle : @"Полностью экологичное!",
                                kNDIntroPageDescription : @"Мы заботимся об экологии, а значит и наше приложение не причинит вред живой природе.",
                                kNDIntroPageImageName : @"Intro_4"
                                },
                              @{kNDIntroPageTitle : @"Давай уже,\nжми на кнопку!",
                                kNDIntroPageImageName : @"Intro_5",
                                kNDIntroPageTitleLabelHeightConstraintValue : @0,
                                kNDIntroPageImageHorizontalConstraintValue : @-40
                                }
                              ];
    self.introView = [[NDIntroView alloc]initWithFrame:self.view.frame
                                         parallaxImage:[UIImage imageNamed:@"ParallaxImage"]
                                               andData:self.pageContentArray];
    self.introView.delegate = self;
    [self.view addSubview:self.introView];
    [self.introView.lastPageButton setTitle:@"Поехали!" forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NDIntro delegate
-(void)launchAppButtonPressed {
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:1] forKey:FIRST_LAUNCH_KEY];
    [(AppDelegate*)([UIApplication sharedApplication]).delegate updateAppState];
}

#pragma mark - Other methods
-(NSArray*)generateImageWithHeight:(CGFloat)height{
    NSMutableArray* result = [[NSMutableArray alloc]init];
    FAKFontAwesome* icon = [[FAKFontAwesome alloc] init];
   
    icon = [FAKFontAwesome homeIconWithSize:height];
    [icon addAttribute:NSForegroundColorAttributeName value:[Helper colorFromHexString:WHITE_TEXT_COLOR]];
    [result addObject:[icon imageWithSize:CGSizeMake(height, height)]];
    
    icon = [FAKFontAwesome carIconWithSize:height];
    [icon addAttribute:NSForegroundColorAttributeName value:[Helper colorFromHexString:WHITE_TEXT_COLOR]];
    [result addObject:[icon imageWithSize:CGSizeMake(height, height)]];
    
    icon = [FAKFontAwesome starIconWithSize:height];
    [icon addAttribute:NSForegroundColorAttributeName value:[Helper colorFromHexString:WHITE_TEXT_COLOR]];
    [result addObject:[icon imageWithSize:CGSizeMake(height, height)]];
    
    icon = [FAKFontAwesome calendarIconWithSize:height];
    [icon addAttribute:NSForegroundColorAttributeName value:[Helper colorFromHexString:WHITE_TEXT_COLOR]];
    [result addObject:[icon imageWithSize:CGSizeMake(height, height)]];
   
    return [NSArray arrayWithArray:result];
}

@end
