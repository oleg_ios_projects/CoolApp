//
//  UIStoryboard+Instant.h
//  A3mobile
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UIGravityFun.h"
#import "UIMainMenu.h"
#import "UIIntro.h"

@interface UIStoryboard (Instant)
+ (UIStoryboard*) mainStoryboardIstance;
+ (UIStoryboard*) authorizationStoryboardIstance;
+ (UIStoryboard*) funStoryboardIstance;

+ (UINavigationController*)instantAuthorizationNC;
+ (UINavigationController*)instantFunNC;

+ (UIMainMenu*)instantMainMenuVC;
+ (UIIntro*)instantIntroVC;
+ (UIGravityFun*)instantGravityFunVC;



@end
