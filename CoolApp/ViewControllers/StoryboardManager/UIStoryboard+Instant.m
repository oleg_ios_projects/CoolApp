//
//  UIStoryboard+Instant.m
//  A3mobile
//
//  Created by iMAC on 01.07.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIStoryboard+Instant.h"

static UIStoryboard* mainStoryboard;
static UIStoryboard* authStoryboard;
static UIStoryboard* funStoryboard;

@implementation UIStoryboard (Instant)

#pragma mark - Storyboards
+ (UIStoryboard*) mainStoryboardIstance
{
    if(mainStoryboard == nil)
    {
        mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    }
    return mainStoryboard;
}

+ (UIStoryboard*) authorizationStoryboardIstance
{
    if(authStoryboard == nil)
    {
        authStoryboard = [UIStoryboard storyboardWithName:@"Authorization" bundle:nil];
    }
    return authStoryboard;
}

+ (UIStoryboard*) funStoryboardIstance
{
    if(funStoryboard == nil)
    {
        funStoryboard = [UIStoryboard storyboardWithName:@"Fun" bundle:nil];
    }
    return funStoryboard;
}


#pragma mark - NavigationControllers
+ (UINavigationController*)instantAuthorizationNC
{
    return [[UIStoryboard storyboardWithName:@"Authorization" bundle:nil] instantiateInitialViewController];
}

+ (UINavigationController*)instantFunNC
{
    return [[UIStoryboard storyboardWithName:@"Fun" bundle:nil] instantiateInitialViewController];
}


#pragma mark - Instants
#pragma mark - Global
+ (UIMainMenu*)instantMainMenuVC {
    return [[UIStoryboard mainStoryboardIstance] instantiateViewControllerWithIdentifier:@"UIMainMenu"];
}
#pragma mark - Authorized
+ (UIIntro*)instantIntroVC {
    return [[UIStoryboard authorizationStoryboardIstance] instantiateViewControllerWithIdentifier:@"UIIntro"];
}

#pragma mark - Fun
+ (UIGravityFun *)instantGravityFunVC {
    return [[UIStoryboard funStoryboardIstance] instantiateViewControllerWithIdentifier:@"UIGravityFun"];
}



@end
