//
//  UIMainMenu.m
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import "UIMainMenu.h"
#import "HeaderFilesControllers.h"

@interface UIMainMenu ()

@end

@implementation UIMainMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateDataArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.dataArray count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[[self.dataArray objectAtIndex:section] objectForKey:KEY_SECTION_CONTENT] count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableDictionary* dsItem = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:KEY_SECTION_CONTENT] objectAtIndex:indexPath.row];
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"reuseMenu"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"reuseMenu"];
    }
    cell.textLabel.text = [dsItem objectForKey:KEY_CELL_TITLE];
    //cell.detailTextLabel.text = [dsItem objectForKey:KEY_CELL_VALUE];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Other methods
-(void)updateDataArray {
    NSMutableArray* section = [NSMutableArray array];
    NSMutableArray* ds = [NSMutableArray array];
    [ds addObject:[NSMutableDictionary dictionaryWithDictionary:@{KEY_CELL_TITLE:@"Гравитация", }]];
    
    [section addObject:@{KEY_SECTION_TITLE:@"",KEY_SECTION_CONTENT:ds}];
    self.dataArray = [NSArray arrayWithArray:section];
}

@end
