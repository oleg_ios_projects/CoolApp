//
//  UIMainMenu.h
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIMainMenu : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
