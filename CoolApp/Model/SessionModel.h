//
//  SessionModel.h
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SessionModel : NSObject

@property (nonatomic) NSString* Identifier;
@property (nonatomic) NSDate* ExpierDate;
@property (nonatomic) BOOL IsActive;

@end
