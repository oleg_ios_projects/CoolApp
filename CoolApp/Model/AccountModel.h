//
//  AccountModel.h
//  CoolApp
//
//  Created by iMAC on 19.09.16.
//  Copyright © 2016 platforma-soft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SessionModel.h"

@interface AccountModel : NSObject

@property (nonatomic) SessionModel* Session;
@property (nonatomic) NSString* Username;


@end
